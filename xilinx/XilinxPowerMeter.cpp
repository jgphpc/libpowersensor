#include "../common/PowerMeter.h"

#include "XilinxPowerSensor.h"

int main(int argc, char **argv)
{
    auto sensor = powersensor::xilinx::XilinxPowerSensor::create();
    run(sensor, argc, argv);
    delete sensor;
}