#include <unistd.h>
#include <omp.h>
#include <dirent.h>
#include <string.h>

#include <cstdio>
#include <cstdlib>
#include <string>

#include <sstream>
#include <vector>
#include <iostream>

#include "XilinxPowerSensor.h"

namespace powersensor {
namespace xilinx {

class XilinxPowerSensor_ : public XilinxPowerSensor {
    public:
        XilinxPowerSensor_(int device_number);

    private:
        class XilinxState{
            public:
                operator State();
                double timeAtRead;
                double instantaneousPower   = 0;
                double consumedEnergyDevice = 0;
        };

        virtual State measure();

        virtual const char* getDumpFileName() {
            return "/tmp/xilinxpowersensor.out";
        }

        virtual int getDumpInterval() {
            return 100; // milliseconds
        }

        std::string filename;

        XilinxState previousState;
        XilinxState read_xilinx();
};

XilinxPowerSensor_::XilinxState::operator State()
{
    State state;
    state.timeAtRead = timeAtRead;
    state.joulesAtRead = consumedEnergyDevice * 1e-6;
    return state;
}

XilinxPowerSensor* XilinxPowerSensor::create(
    int device_number)
{
    return new XilinxPowerSensor_(device_number);
}

XilinxPowerSensor_::XilinxPowerSensor_(
    int device_number)
{
    char* c_str_filename = std::getenv("POWERSENSOR_DEVICE");
    if (c_str_filename) {
        filename = c_str_filename;
    } else {
        fprintf(stderr, "No POWERSENSOR_DEVICE specified.\n");
        exit(1);
    }

    previousState = read_xilinx();
    previousState.consumedEnergyDevice = 0;
}

float get_power(std::string& filename)
{
    // Open power file
    // e.g. /sys/devices/pci0000:a0/0000:a0:03.1/0000:a1:00.0/hwmon/hwmon2/power1_input
    std::ifstream file(filename, std::ios::in | std::ios::binary );
    if (errno != 0) {
        std::cerr << "Could not open: " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    // This file has one line with instantenous power consumption in uW
    std::string line;
    std::getline(file, line);
    try {
        return stoi(line);
    } catch (std::invalid_argument& e) {
        std::cerr << "Could not parse: " << line << std::endl;
        exit(EXIT_FAILURE);
    }
}

XilinxPowerSensor_::XilinxState XilinxPowerSensor_::read_xilinx() {
    XilinxState state;
    state.timeAtRead = get_wtime();
    state.instantaneousPower = get_power(filename);
    state.consumedEnergyDevice = previousState.consumedEnergyDevice;
    float averagePower = (state.instantaneousPower + previousState.instantaneousPower) / 2;
    float timeElapsed = (state.timeAtRead - previousState.timeAtRead);
    state.consumedEnergyDevice += averagePower * timeElapsed;
    previousState = state;
    return state;
}

State XilinxPowerSensor_::measure() {
    return read_xilinx();
}

} // end namespace xilinx
} // end namespace powersensor
