#include "../common/PowerMeter.h"

#include "NVMLPowerSensor.h"

int main(int argc, char **argv)
{
    auto sensor = powersensor::nvml::NVMLPowerSensor::create();
    run(sensor, argc, argv);
    delete sensor;
}