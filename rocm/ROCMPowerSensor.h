#ifndef ROCM_POWER_SENSOR_H
#define ROCM_POWER_SENSOR_H

#include "PowerSensor.h"

namespace powersensor {
    namespace rocm {
        class ROCMPowerSensor : public PowerSensor {
            public:
                static ROCMPowerSensor* create(
                    int device_number = 0);
        };
    } // end namespace rocm
} // end namespace powersensor

#endif
