#ifndef AMDGPU_POWER_SENSOR_H
#define AMDGPU_POWER_SENSOR_H

#include "PowerSensor.h"

namespace powersensor {
    namespace amdgpu {
        class AMDGPUPowerSensor : public PowerSensor {
            public:
                static AMDGPUPowerSensor* create(
                    int device_number = 0);
        };
    } // end namespace amdgpu
} // end namespace powersensor

#endif
