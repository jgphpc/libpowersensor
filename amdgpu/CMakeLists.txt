project(amdgpu)

add_library(
    powersensor-amdgpu
    OBJECT
    AMDGPUPowerSensor.cpp
)

target_link_libraries(
    powersensor-amdgpu
    Threads::Threads
)

install(
    FILES
    AMDGPUPowerSensor.h
    DESTINATION
    include/powersensor
)

add_executable(
    AMDGPUPowerMeter
    AMDGPUPowerMeter.cpp
)

target_link_libraries(
    AMDGPUPowerMeter
    $<TARGET_OBJECTS:powersensor-common>
    powersensor-amdgpu
)

install(
    TARGETS
    AMDGPUPowerMeter
    RUNTIME DESTINATION bin
)

target_link_libraries(
    powersensor
    PUBLIC
    powersensor-amdgpu
)
