#ifndef ARDUINO_POWER_SENSOR_H_
#define ARDUINO_POWER_SENSOR_H_

#include "PowerSensor.h"

namespace powersensor {
    namespace arduino {
        class ArduinoPowerSensor : public PowerSensor {
            public:
                static ArduinoPowerSensor* create(
                    const char *device = default_device().c_str());

                static std::string default_device() {
                    return "/dev/ttyACM0";
                }
        };
    } // end namespace arduino
} // end namespace powersensor

#endif
