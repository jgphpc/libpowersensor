#include "../common/PowerMeter.h"

#include "ArduinoPowerSensor.h"

int main(int argc, char *argv[])
{
    auto sensor = powersensor::arduino::ArduinoPowerSensor::create();
    run(sensor, argc, argv);
    delete sensor;
}