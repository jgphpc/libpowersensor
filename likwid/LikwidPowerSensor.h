#ifndef LIKWID_POWER_SENSOR_H
#define LIKWID_POWER_SENSOR_H

#include "PowerSensor.h"

namespace powersensor {
    namespace likwid {
        class LikwidPowerSensor : public PowerSensor {
            public:
                static LikwidPowerSensor* create();
        };
    } // end namespace likwid
} // end namespace powersensor

#endif
