#include "../common/PowerMeter.h"

#include "JetsonPowerSensor.h"

int main(int argc, char **argv)
{
    auto sensor = powersensor::jetson::JetsonPowerSensor::create();
    run(sensor, argc, argv);
    delete sensor;
}