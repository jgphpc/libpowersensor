#ifndef JETSON_POWER_SENSOR_H
#define JETSON_POWER_SENSOR_H

#include "PowerSensor.h"

namespace powersensor {
    namespace jetson {
        class JetsonPowerSensor : public PowerSensor {
            public:
                static JetsonPowerSensor* create();
        };
    } // end namespace jetson
} // end namespace powersensor

#endif
