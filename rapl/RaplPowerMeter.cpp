#include "../common/PowerMeter.h"

#include "RaplPowerSensor.h"

int main(int argc, char **argv)
{
    auto sensor = powersensor::rapl::RaplPowerSensor::create();
    run(sensor, argc, argv);
    delete sensor;
}